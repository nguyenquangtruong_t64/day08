<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Danh sách sinh viên</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="new.css">
</head>

<body>
    <?php
	$_SESSION['department'] = '';
	$_SESSION['keyWord'] = '';

    if (!empty($_POST['btnDelete'])) {
		$_SESSION['department'] =  '';
		$_SESSION['keyWord'] = '';
	}

	if (!empty($_POST['btnSearch'])) {
		$_SESSION['department'] = isset($_POST['department']) ? $_POST['department'] : '';
		$_SESSION['keyWord'] = isset($_POST['keyWord']) ? $_POST['keyWord'] : '';
	}
	?>

    <fieldset class="background">
        <form action="" method="POST" id="form" enctype="multipart/form-data">

            <div class="search-bar">
                <div class="list-form">
                    <p class="list-form-text">
                        Khoa
                    </p>

                    <select name='department'>
                    <?php 
                        $depart = array("EMPTY"=>"", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                        foreach ($depart as $key => $value) {
                            if ($key == $_SESSION['department']) {
                                echo '<option value="' . $key . '" selected="selected">' . $value . '</option>';
                            } 
                            else {
                                echo '<option value="' . $key . '">' . $value . '</option>';				
                            }
                        }
                    ?>
                    </select>
                </div>

                <div class="list-form">
                    <p class="list-form-text">
                        Từ khóa
                    </p>
                    <?php
                     echo '<input type="text" name="keyWord" id="keyWord" class="input-text" value="' . $_SESSION['keyWord'] . '">'
					?>
                </div>

                <div class="list-form list-search">
                    <input type="submit" value="Tìm kiếm" name="btnSearch" class="btnSearch">
                    <input type="submit" value="Xóa" name="btnDelete" class="btnSearch btnDelete"
                        onclick="deleteData()">
                </div>
            </div>

            <div class="search-res">
                <p>Số sinh viên tìm thấy: XXX</p>
            </div>

            <div class="list-add">
                <input type="submit" class="btnAdd" name="btnAdd" value="Thêm" />
            </div>

            <div class="list-student">
                <table>
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <button class="btnAction">Xóa</button>
                            <button class="btnAction">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Trần Thị B</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <button class="btnAction">Xóa</button>
                            <button class="btnAction">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Nguyễn Hoàng C</td>
                        <td>Khoa học vật liệu</td>
                        <td>
                            <button class="btnAction">Xóa</button>
                            <button class="btnAction">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Đinh Quang D</td>
                        <td>Khoa học vật liệu</td>
                        <td>
                            <button class="btnAction">Xóa</button>
                            <button class="btnAction">Sửa</button>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </fieldset>

    <script>
    function deleteData() {
        document.getElementById('department').value = '';
        document.getElementById('keyWord').value = '';
    }
    </script>

</body>

</html>
